#!/bin/sh

# Replace shipped minified javascript files with own generated ones
# That makes sure that we have the correct sources for the minified versions.

set -e
set -x

uglifyjs --compress --mangle -o ${BUILD_DIR}/onionshare_cli/resources/static/js/socket.io.min.js -- ${CURDIR}/debian/missing-sources/socket.io.js
uglifyjs --compress --mangle -o ${BUILD_DIR}/onionshare_cli/resources/static/js/jquery-3.5.1.min.js -- ${CURDIR}/debian/missing-sources/jquery-3.5.1.js
