There are two possibilities to use OnionShare in Debian.
By default, OnionShare will expect a running Tor instance on your system, on
ports 9151, 9153 or 9051. OnionShare does not ship nor run its own Tor
instance.

This means that either:
- you have manually installed TorBrowser or installed the torbrowser-launcher
  package from Debian repositories. In both cases, you need to run TorBrowser,
  so that OnionShare uses this Tor instance. This is the default behavior.

Or:
- you can configure access to the control port of a system-wide Tor, and point
  OnionShare to it.
